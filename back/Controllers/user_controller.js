var requestJSON = require('request-json');
const URL_BASE='/apitechu/v1/';
const URLBASEDB= 'https://api.mlab.com/api/1/databases/techu31db/collections/';
const queryStringField = '&f={"_id":0}';

require('dotenv').config();
const API_KEY = '&apiKey=' + process.env.MLAB_API_KEY;
const API_KEYS = 'apiKey=' + process.env.MLAB_API_KEY;

// Función del Metodo GET para conseguir todos los usuarios

function getUsers(req,res){
            console.log('GET' + URL_BASE + 'usersM');
            var httpClient = requestJSON.createClient(URLBASEDB);
            //console.log("Cliente HTTP mLAB creado");
            var queryString = 'f={"_id":0}&s={"last_name":1}&l=10';
            var url_total = URLBASEDB + 'userAccount?' + API_KEYS;
            //console.log(url_total);
            httpClient.get(url_total,
              function(err,respuestaMLAB,body){
                var response = {};
                if(err){
                  response = {"mensaje":"Error obteniendo usuario"};
                  res.status(500);
                } else{
                  if(body.length > 0){
                    response = body;
                  } else{
                    response = {"mensaje":"Ningún elemento 'user'."};
                    res.status(404);
                  }
                }
                res.send(response);
              });
};

module.exports.getUsers = getUsers;

// Función del Metodo GET para conseguir por ID de Usuario

function getUsersID(req,res){
            console.log('GET' + URL_BASE + 'usersM/:id');
            var id= req.params.id;
            var queryString = 'q={"id":' + id + '}';
            //var queryStringField = 'f={"_id":0}&s={"last_name":1}&l=10';
            var url_total = URLBASEDB + 'userAccount?' + queryString + queryStringField + API_KEY;
            //console.log(url_total);
            var httpClient = requestJSON.createClient(URLBASEDB);
            //console.log("Cliente HTTP mLAB creado");
            httpClient.get(url_total,
              function(err,respuestaMLAB,body){
                var response = {};
                if(err){
                  response = {"mensaje":"Error obteniendo usuario"};
                  res.status(500);
                } else{
                  if(body.length > 0){
                    response = body;
                  } else{
                    response = {"mensaje":"Ningún elemento 'user'."};
                    res.status(404);
                  }
                }
                res.send(response);
              });
};

module.exports.getUsersID = getUsersID;

// Función del Metodo POST para registrar un nuevo Usuario

function setUser(req, res){
   var clienteMlab = requestJSON.createClient(URLBASEDB);
   //console.log(req.body);
   var queryString = 'c=true';
   var url_total = URLBASEDB + 'userAccount?' + queryString + queryStringField + API_KEY;
   //console.log(url_total);
   clienteMlab.get(url_total,
     function(error, respuestaMLab, body){
       //console.log(body);
       var newID= new Number(body);
       newID = body + 1;
       //console.log(newID);
       var newUser = {
         "id" : newID,
         "first_name" : req.body.first_name,
         "last_name" : req.body.last_name,
         "email" : req.body.email,
         "password" : req.body.password
       };
       clienteMlab.post(URLBASEDB + "userAccount?" + API_KEY, newUser,
         function(error, respuestaMLab, body){
           //console.log(body);
           res.status(201);
           res.send(body);
         });
     });
 };

module.exports.setUser = setUser;

// Función del Metodo PUT con id de mLab (_id.$oid)

function updateUser(req, res) {
     var id = req.params.id;
     let userBody = req.body;
     var queryString = 'q={"id":' + id + '}&';
     var httpClient = requestJSON.createClient(URLBASEDB);
     var url_total = URLBASEDB + 'userAccount?' + queryString + API_KEY;
     //console.log(url_total);
     httpClient.get(url_total,
       function(err, respuestaMLab, body){
         let response = body[0];
         // console.log(body);
         // console.log(body.length);
         if(body.length>0){
           //Actualizo campos del usuario
           // let updatedUser = {
           //   "id" : req.body.id,
           //   "first_name" : req.body.first_name,
           //   "last_name" : req.body.last_name,
           //   "email" : req.body.email,
           //   "password" : req.body.password
           // };
           //Otra forma simplificada (para muchas propiedades)
           var updatedUser = {};
           Object.keys(response).forEach(key => updatedUser[key] = response[key]);
           Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
           // PUT a mLab
           httpClient.put(URLBASEDB + 'userAccount/' + response._id.$oid + '?' + API_KEYS, updatedUser,
             function(err, respuestaMLab, body){
               var response = {};
               if(err) {
                   response = {
                     "mensaje" : "Error actualizando usuario."
                   }
                   res.status(500);
               } else {
                 if(body.length > 0) {
                   response = body;
                 } else {
                   response = {
                     "mensaje" : "Usuario actualizado correctamente."
                   }
                   res.status(200);
                 }
               }
             });
         }else{
           response = {
             "mensaje" : "Usuario no existe"
           }
           res.status(500);
         }
         res.send(response);
       });
 };

 module.exports.updateUser = updateUser;

// Función del Metodo DELETE user with id

function deleteUser(req, res){
   var id = req.params.id;
   var queryStringID = 'q={"id":' + id + '}&';
   console.log(URLBASEDB + 'userAccount?' + queryStringID + API_KEY);
   var httpClient = requestJSON.createClient(URLBASEDB);
   httpClient.get(URLBASEDB + 'userAccount?' +  queryStringID + API_KEY,
     function(error, respuestaMLab, body){
       var respuesta = body[0];
       //console.log(respuesta.length);
       if(respuesta!=undefined){
         httpClient.delete(URLBASEDB + "userAccount/" + respuesta._id.$oid +'?'+ API_KEY,
           function(error, respuestaMLab,body){
             var mensaje={
               "mensaje":"Usuario eliminado con éxito",
               "usuario":body
             }
             res.send(mensaje);
             res.status(200);
         });
       }else{
         var mensaje={
           "mensaje":"Usuario no encontrado"
         }
         res.send(mensaje);
         res.status(400);
       }
     });
 };

 module.exports.deleteUser = deleteUser;

// Función del MÉTODO POST login

function loginUser(req, res){
   let email = req.body.email;
   let pass = req.body.password;
   let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}';
   //let limFilter = 'l=1&';
   let clienteMlab = requestJSON.createClient(URLBASEDB); // Existe un usuario que cumple 'queryString'
   let login = '{"$set":{"logged":true}}';
           clienteMlab.put(URLBASEDB + 'userAccount?' + queryString + API_KEY, JSON.parse(login),
           //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
             function(errPut, resPut, bodyPut) {
               var reslogin = bodyPut.n;
               //console.log(reslogin);
               if(reslogin==1){
                 res.send({'mensaje':'Login correcto', 'user':email});
                 res.status(200);
               }else{
                 res.send({'mensaje':'Alguno de los datos ingresado es incorrecto. Verifique e intente nuevamente.', 'user':email});
                 res.status(404);
               }
             });
};

module.exports.loginUser = loginUser;

//Función del método POST logout

function logoutUser(req, res){
   let email = req.body.email;
   let queryString = 'q={"email":"' + email + '"}';
   let clienteMlab = requestJSON.createClient(URLBASEDB);
   let logout = '{"$unset":{"logged":true}}';
   var url_total = URLBASEDB + 'userAccount?' + queryString + queryStringField + API_KEY;
   clienteMlab.get(url_total,
     function(err,respuestaMLAB,body){
       if(err){
         response = {"msg":"Error obteniendo usuario"};
         res.status(500);
       } else{
         if(body.length > 0){
           var response = body[0].logged;
           //console.log(response);
           if(response!=undefined){
             clienteMlab.put(URLBASEDB + 'userAccount?' + queryString + API_KEY, JSON.parse(logout),
               function(errPut, resPut, bodyPut) {
                 var reslogin = bodyPut.n;
                 //console.log(reslogin);
                 if(reslogin==1){
                   res.send({'msg':'Logout correcto', 'user':email});
                   res.status(200);
                 }else{
                   res.send({'msg':'Logout incorrecto', 'user':email});
                 }
               });
           }else{
             res.send({"mensaje":"Usuario no autenticado"});
             res.status(404);
           }
         } else{
           res.send({"mensaje":"No existe el usuario"});
           res.status(404);
         }
       }
     });
   //console.log(queryString);
   //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
};

module.exports.logoutUser = logoutUser;
