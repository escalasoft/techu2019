var express = require('express');
var userFile = require('./user.json');
var totalUsers = 0;
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var requestJSON = require('request-json');
const URL_BASE='/apitechu/v1/';
const PORT = process.env.PORT || 3000;
const user_controller = require('./controllers/user_controller');

//Todas son peticiones asincronas
//get es un metodo, necesita la URI para devolver la peticion http
//callback es una funcion anima que esta como parametro dentro de otra funcion.

//Función para implementar la peticion GET de todos los 'users' (Collections)

app.get(URL_BASE+'users',
  function(request, response) {
    console.log('GET'+URL_BASE+'users');
    response.send(userFile);
  });

//Función para implementar la peticion GET de un 'users' (Instancia)

app.get(URL_BASE+'users/:id',
  function(request,response) {
    console.log('GET'+URL_BASE+'users/id');
    let indice = request.params.id;
    let instancia = userFile[indice-1];
    let respuesta = (instancia!= undefined) ? instancia:{"mensaje":"Recurso no encontrado"};
   response.status(200);
   response.send(respuesta);

    //response.send(userfile);
});


// Función para implementar la peticion GET con Query String

app.get(URL_BASE + 'usersq',
           function(req,res){
             console.log('GET' + URL_BASE + 'con query String');
             console.log(req.query);
             let respuesta=req.query;
             res.send(respuesta);
});

// Funcion para Inplementar la Peticion POST

app.post(URL_BASE + 'users',
           function(req,res){
             totalUsers = userFile.length + 1;
             cuerpo = req.body;
             console.log(cuerpo.length);
             if(cuerpo!=undefined){
             let newUser={
               userId : totalUsers,
               firstName : req.body.first_name,
               lastName : req.body.last_name,
               email : req.body.email,
               password : req.body.password
               }
               userFile.push(newUser);
               res.status(200);
               res.send(
               {
                 "mensaje" : "Usuario creado con éxito",
                 "usuario": newUser,
                 "usuarios" : userFile
             });
           }else{
              res.status(404);
              res.send(
                {
                 "mensaje" : "Body vacío"
             });
           }
});

//Funcion para implementar la Peticion PUT

app.put(URL_BASE + 'users/:id',
           function(req,res){
               let instancia = userFile[req.params.id - 1];
               let respuesta=(instancia != undefined) ? {"mensaje":"Usuario actualizado con éxito"}:{"mensaje":"Recurso no encontrado"};
                  if(instancia!=undefined){
                    userFile[req.params.id - 1].first_name=req.body.first_name,
                    userFile[req.params.id - 1].last_name=req.body.last_name,
                    userFile[req.params.id - 1].email=req.body.email,
                    userFile[req.params.id - 1].password=req.body.password
                 res.status(201);
                 res.send(
                 {
                   "mensaje" : respuesta,
                   "usuario": instancia,
                   "usuarios" : userFile
                 });
                }else{
                 res.status(404);
                 res.send(respuesta);
               }
});


// Funcion para implementar la Peticion DELETE

app.delete(URL_BASE+'users/:id',
  function(request,response) {
    let indice = request.params.id - 1;
    let instancia = userFile[indice];
    let respuesta = (instancia!= undefined) ? {"mensaje":"Recurso eliminado con éxito"}:{"mensaje":"Recurso no encontrado"};
    if(instancia != undefined){
      userFile.splice(indice,1);
    response.status(200);
    response.send(
      {
        "mensaje":respuesta,
        "usuario": instancia,
        "usuarios": userFile
      });
    }else{
      response.status(404);
      response.send(respuesta);
    }
  });

  app.listen(PORT);

   // notas: ctrl+shift+D se duplica un registro

   // Implementación de LOGIN (petición POST)

   app.post(URL_BASE + 'login',
      function (request,response) {
        cuerpo = req.body;
        if (cuerpo = !undefined){
          var user = req.body.email;
          var pas = req.body.password;
          var flag=false;

          for(var us of userFile) {
            if (cuerpo !=undefined) {
              us.logget = true;
              flag=true;
              writeUserDataToFile(userFile);
              res.estatus(200);
              res.send(
                {
                  "mensaje" : "Login con éxito"
                });
                  break;
            }
          } 
        }
        if(!flag){
          res.status(400);
          res.send(
          {
            "mensaje" : "Su email y/o password son incorrectos- Intente nuevamente"
            });
        }
      else{
        res.status(400);
        res.send(
        {
          "mensaje" : "body vacío"
        });
      }
 });


 // Implementación de LOGOUT (petición POST)

   app.post(URL_BASE + 'logout',
      function(req,res){
        cuerpo=req.body;
        if(cuerpo !=undefined){
          var user=req.body.email;
          var flag=false;
        for(var us of userFile){
          if(us.email == user){
            if(us.logged !=undefined){
              delete us.logged;
              flag=true;
              writeUserDataToFile(userFile);
              res.status(200);
              res.send(
              {
                "mensaje" : "logout con éxito"
              });
                break;
              }  
            }
          }
          if(!flag){
            res.status(400);
            res.send(
          {
            "mensaje" : "usuario no logueado"
          });
        }
      }else{
        res.status(404);
        res.send(
      {
        "mensaje" : "body vacío"
      });
    }
});  

// Funcion GET para consumir API REST desde MLab

app.get(URL_BASE + 'usersM', user_controller.getUsers);

app.get(URL_BASE + 'usersM/:id', user_controller.getUsersID);

app.post(URL_BASE + 'usersM', user_controller.setUser);

app.put(URL_BASE + 'usersM/:id', user_controller.updateUser);

app.delete(URL_BASE + 'usersM/:id', user_controller.deleteUser);

app.post(URL_BASE + 'loginM', user_controller.loginUser);

app.post(URL_BASE + 'logoutM', user_controller.logoutUser);

//Método para escribir en un archivo físico

function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 }

app.listen(PORT, function(){
  console.log('escuchandooooo p 3000');
});


// Funcion para hacer pruebas unitarias

function() { //Función manejadora de la suite
  it ('Prueba que la API devuelve una lista de usuarios correctos', //Test unitario
    function(done){
        chai.request('http://localhost:3000')
            .get('/colapi/v3/users')
            .end(
              function(err, res){
                res.should.have.status(200);
                res.body.should.be.a('array');
                for (users of res.body){
                  users.should.have.property('firstname');
                  users.should.have.property('last_name');
                }
                done();
              })
    })
});

   
   
